# This is a library module.

# Created By BlueBaritone21 <bluebaritone21[at]duck[dot]com> as part of
# the Baritone Game Engine (BariGame)

# This software is provided with ABSOLUTELY NO WARRANTY, AS PERMITTED BY APPLICABLE LAW,
# INCLUDING THE IMPLIED WARRANTY OF MERCHANTABILITY.

# This software is licensed under the GPL 2.0 or higher. You can find the full text online at
# <https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
# 🄯 2024 BlueBaritone21.

from math import tau      # I like Tau better than Pi.
from math import sin, cos # TRIG!!!
from time import sleep
from os import system as sys
from copy import deepcopy

def clear():
    sys("clear")

class angle:
    __angle = 180
    def __init__(self, radians: float):
        self.__angle = radians * 360/tau
    
    def radians(self):
        return degToRad(self.__angle)
    
    def degrees(self):
        return self.__angle

    def __str__(self):
        return str(self.degrees()) + "°"


def degToRad(degs: float):
    return degs * (tau/360)

class vector:
    dir = angle(tau/4)
    mag = 0
    def __init__(self, angle: angle, amount: int):
        self.dir = angle
        self.mag = amount
    def getDeltas(self):
        return pos( cos(self.dir.radians())*self.mag, sin(self.dir.radians()) * self.mag)
    def __str__(self):
        return str(self.getDeltas())


class pos:
    x = 0
    y = 0
    def __init__(self, x: int,y: int):
        self.x = x
        self.y = y
    
    def __str__(self):
        return "(" + str(self.x) +","+ str(self.y) + ")"


class sprite:
    pos  = pos(0,0)
    icon = "⚠"
    vel  = vector(0,0)


    def __init__(self, loc: pos, icon: str, vel:vector=vector(0,0)):
        self.pos = loc
        self.vel = vel
        self.icon = icon[0] if len(icon)>0 else "⚠"


    def accelerate(self, amount: int):
        self.vel = vector(self.vel.dir.radians(),self.vel.mag+amount)


    def turn(self, radians: float):
        self.vel.dir = angle(self.vel.dir.radians+radians)


    def step(self, top:int=999, right:int=999):
        x = int(self.pos.x+round(self.vel.getDeltas().x))
        x = x if x > -1 and x < right else self.pos.x
        y = int(self.pos.y+round(self.vel.getDeltas().y))
        y = y if y > -1 and y < top else self.pos.y
        self.pos = pos(x,y)
    
    def __str__(self):
        return "sprite \""+self.icon+"\" located at ("+self.pos.x+","+self.pos.y+"), rotated"+self.vel.dir.degrees()+"°, and traveling at"


class display:
    screen  = []
    sprites = {}

    def __init__(self, width: int, height: int, initItem:str="~"):
        for i in range(0 , height):
            _line = []
            for j in range(0, width):
                _line.append(initItem)
            self.screen.append(_line)
    
    def setTile(self, pos:pos, tile:str):
        self.screen[len(self.screen)-pos.y-1][pos.x] = tile

    def getTile(self, pos:pos):
        return self.screen[len(self.screen)-pos.y-1][pos.x]

    def addSprite(self, name:str, sprite:sprite):
        self.sprites[name]=sprite
    
    def getSprite(self, name:str):
        return self.sprites[name]

    def delSprite(self, name:str):
        del self.sprites[name]
    
    def step(self):
        for sprite in self.sprites.values():
            sprite.step()

    def __str__(self):
        text = ""
        __copy = deepcopy(self.screen)

        for i in self.sprites.values():
            __copy[len(__copy)-i.pos.y-1][i.pos.x] = i.icon
        for i in range(0, len(__copy)):
            for j in range(0, len(__copy[0])):
                text = text + __copy[i][j]
            text = text + "\n"
        return text

class scene:
    setup    = lambda: print("Hi!")
    loop     = lambda: print("looping!")
    end      = False
    getInput = lambda: input("Input: ")
    def __init__(self,setup=lambda: print("Welcome to my blank scene!"),loop = lambda: print("looping!"), interact = lambda: input("Input: ")):
        self.setup    = setup
        self.loop     = loop
        self.getInput = interact
        self.end      = False
    def run(self, stage:display):
        self.setup()
        input = ""
        while not self.end:
            clear()
            self.loop()
            stage.step()
            input = self.getInput()
            

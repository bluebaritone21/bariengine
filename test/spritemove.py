import engine as bari
from time import sleep

disp = bari.display(10,20, initItem=".")
disp.addSprite("Carl",bari.sprite(bari.pos(5,0),"^",vel=bari.vector(bari.angle(90),1)))

def intro():
    print("Hi")
    sleep(2)

def tick():
    print("Hi")

game = bari.scene(
    setup    = lambda:intro(),
    loop     = lambda:tick(),
    interact = sleep(0.1)
)

game.run(disp)